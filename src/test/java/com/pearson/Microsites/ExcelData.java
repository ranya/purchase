package com.pearson.Microsites;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

public class ExcelData {
	//String FilePath = "E:\\Inputsheet\\Userlist.xls";
	String FilePath="src\\Userlist.xls";

	public String ReadStringData(Integer SheetNo, Integer RowNum, Integer CellNum) {
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(new File(FilePath));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		HSSFWorkbook workbook = null;
		try {
			workbook = new HSSFWorkbook(fis);
		} catch (IOException e) {
			e.printStackTrace();
		}
		HSSFSheet sheet = workbook.getSheetAt(SheetNo);
		Cell cell = null;
		cell = sheet.getRow(RowNum).getCell(CellNum);
		if (cell == null) {
			return null;
		} else
			/*
			 * if (cell.getCellType() == Cell.CELL_TYPE_FORMULA) {
			 * System.out.println("Formula is " + cell.getCellFormula()); switch
			 * (cell.getCachedFormulaResultType()) {
			 * 
			 * case Cell.CELL_TYPE_NUMERIC: return cell.getNumericCellValue();
			 * break;
			 * 
			 * case Cell.CELL_TYPE_STRING:
			 * System.out.println("Last evaluated as \"" +
			 * cell.getRichStringCellValue() + "\""); return
			 * cell.getStringCellValue(); // default:
			 * 
			 * } }
			 * 
			 * return cell.getStringCellValue();
			 */
			return cell.getStringCellValue();

	}

	/*public double ReadIntegerData(Integer SheetNo, Integer RowNum,
			Integer CellNum) {
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(new File(FilePath));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		HSSFWorkbook workbook = null;
		try {
			workbook = new HSSFWorkbook(fis);
		} catch (IOException e) {
			e.printStackTrace();
		}
		HSSFSheet sheet = workbook.getSheetAt(SheetNo);
		Cell cell = null;
		cell = sheet.getRow(RowNum).getCell(CellNum);
		if (cell == null) {
			return (Double) null;
		} else {
			return cell.getNumericCellValue();
		}

	}*/

	public void WriteData(Integer SheetNo, Integer RowNum, Integer CellNum,
			String value) {
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(new File(FilePath));
			HSSFWorkbook workbook = null;
			workbook = new HSSFWorkbook(fis);
			HSSFSheet sheet = workbook.getSheetAt(SheetNo);
			Row row = sheet.getRow(RowNum);
			Cell cell = row.createCell(CellNum);
			cell.setCellValue(value);
			fis.close();
			FileOutputStream outFile = null;
			outFile = new FileOutputStream(new File(FilePath));
			workbook.write(outFile);
			outFile.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Integer rowCount(Integer sheetNo) {
		File inputWorkbook = new File(FilePath);
		Workbook w = null;
		Integer rowCount = null;
		try {
			w = Workbook.getWorkbook(inputWorkbook);
		} catch (BiffException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Sheet sheet = w.getSheet(sheetNo);
		rowCount = sheet.getRows();
		return rowCount;
	}

	public void TakeExcelBackup() {
		try {
			File inputWorkbook = new File(FilePath);
			String[] test = FilePath.split("\\.");
			String FilePathNew = test[0] + "_input." + test[1];

			FileInputStream fis = new FileInputStream(inputWorkbook);
			HSSFWorkbook workbook = new HSSFWorkbook(fis);
			fis.close();
			FileOutputStream outFile = null;
			outFile = new FileOutputStream(new File(FilePathNew));
			workbook.write(outFile);
			outFile.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String ReadCellDataInArray(Integer SheetNo, Integer RowNum,
			Integer CellNum) {
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(new File(FilePath));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		HSSFWorkbook workbook = null;
		try {
			workbook = new HSSFWorkbook(fis);
		} catch (IOException e) {
			e.printStackTrace();
		}
		HSSFSheet sheet = workbook.getSheetAt(SheetNo);
		Cell cell = null;
		cell = sheet.getRow(RowNum).getCell(CellNum);
		if (cell == null) {
			return null;
		} else {
			return cell.getStringCellValue();
		}

	}
	
	public String returnStr() {
		BufferedReader br = null;
		String sCurrentLine = null;
		String ans = "";
		try {
			br = new BufferedReader(new FileReader("D:\\testEmailReport.html"));
			while ((sCurrentLine = br.readLine()) != null) {
				//System.out.println("lines:"+sCurrentLine);
				ans=ans+sCurrentLine;
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		//System.out.println("ans:"+ans);
		return ans;
		
	}
}