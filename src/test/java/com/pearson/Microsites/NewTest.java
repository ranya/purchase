package com.pearson.Microsites;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class NewTest {

	WebDriver driver;
	String un;
	int jj;
	String baseurl = "http://test.gepurchase.com/setting.php";
	ExcelData ed = new ExcelData();

	//ExtentReports extent = new ExtentReports("E:\\reports\\Automation_report.html", true);
	ExtentReports extent = new ExtentReports("src\\Automation_report.html", true);
	ExtentTest logger = extent.startTest("Microsite Test report", "Extent report Test report for Microsite");

	@BeforeTest
	public void init() {
		System.setProperty("webdriver.chrome.driver", "E:\\Selenium\\SeleniumDriver\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	@Test(priority = 0)
	public void Settings() {
		driver.get("http://test.gepurchase.com/setting.php");
		logger.log(LogStatus.INFO, "Browser is getting opened");
		Select Microsite = new Select(driver.findElement(By.xpath(".//*[@id='msname']")));
		Microsite.selectByValue("tcs");
		driver.findElement(By.id("sandbox")).click();
		driver.findElement(By.xpath(".//*[@id='settings']/fieldset[4]/fieldset/input")).click();
		logger.log(LogStatus.PASS, "Landed in homepage");
	}

	@Test(priority = 1)
	public void Homepage() {
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		logger.log(LogStatus.INFO, "Homepage is getting opened");
		driver.findElement(By.id("s1")).click();
		logger.log(LogStatus.PASS, "Landed in Pricing page");
		driver.findElement(By.xpath(".//*[@id='buy-link']/a")).click();
			}

	@Test(priority = 2)
	public void Buying() {
		logger.log(LogStatus.INFO, "Buying");
	    driver.findElement(By.id("FNAME")).sendKeys("Saranya");
		driver.findElement(By.id("LNAME")).sendKeys("Thiruvenkadasamy");
		driver.findElement(By.id("EMAIL")).sendKeys("ranyathiru241@gmail.com");

		outfor: for (int ii = 1; ii < 184; ii++) {
			if (ed.ReadStringData(0, ii, 1).equals("not done")) {
				un = ed.ReadStringData(0, ii, 0);
				jj = ii;
				break outfor;
			}
		}

		driver.findElement(By.id("GE_USERNAME")).sendKeys(un);
		driver.findElement(By.id("GE_PASSWORD")).sendKeys("2222");
		driver.findElement(By.id("CONF_GE_PASSWORD")).sendKeys("2222");
		Select Country = new Select(driver.findElement(By.id("COUNTRY")));
		Country.selectByValue("IN");
		driver.findElement(By.id("TCSREFNAME")).sendKeys("tranya");
		driver.findElement(By.id("SECRETCODE")).sendKeys("TC5_Fr1end");
		Select Language = new Select(driver.findElement(By.id("USER1")));
		Language.selectByValue("EN");
		Select Purchase = new Select(driver.findElement(By.id("skus")));
		Purchase.selectByValue("147|187|testcode1234");
		driver.findElement(By.id("register-btn")).click();
		logger.log(LogStatus.PASS, "Registration completed");
		ed.WriteData(0, jj, 1, "done");
	}

	@Test(priority = 3)
	public void purchase() {
		logger.log(LogStatus.INFO, "Purchase");
		driver.findElement(By.id("cc_number")).sendKeys("378282246310005");
		driver.findElement(By.id("expdate_month")).sendKeys("11");
		driver.findElement(By.id("expdate_year")).sendKeys("22");
		driver.findElement(By.id("first_name")).sendKeys("Saranya");
		driver.findElement(By.id("last_name")).sendKeys("Thiruvenkadasamy");
		driver.findElement(By.id("btn_pay_cc")).click();
		logger.log(LogStatus.PASS, "Payment completed");
	}

	@Test(priority = 4)
	public void welcomepage() throws InterruptedException {
		driver.manage().window().maximize();
		Thread.sleep(2000);
		logger.log(LogStatus.INFO, "Edge Welcomepage");
		driver.findElement(By.xpath(".//*[@id='contPaymentOptionsContent']/p[4]/a")).click();
		// Alert alert = driver.switchTo().alert();
		// alert.accept();
		driver.findElement(By.xpath(".//*[@id='wrapper']/div/div/div[4]/a")).click();
		driver.findElement(By.id("submit")).click();
		Thread.sleep(2000);
		driver.findElement(By.id("submit1")).click();
		Thread.sleep(2000);
		driver.findElement(By.id("submit2")).click();
		Thread.sleep(2000);
		driver.findElement(By.id("submit3")).click();
		Thread.sleep(2000);
		driver.findElement(By.id("studyplan")).click();
		Thread.sleep(2000);
		driver.findElement(By.id("selfplace")).click();
		driver.findElement(By.id("submit4")).click();
		Thread.sleep(2000);
		driver.findElement(By.id("userLevel")).click();
		Thread.sleep(2000);
		logger.log(LogStatus.PASS, "Landed in Edge");
		driver.findElement(By.xpath(".//*[@id='settings_menu']/a/em")).click();
		Thread.sleep(2000);
		driver.findElement(By.partialLinkText("Log Out")).click();
	
		extent.endTest(logger);
		extent.flush();
	}

	@AfterTest
	public void end_session() {
		driver.quit();

	}
}
